/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Helper;

import java.util.Random;

/**
 *
 * @author Administrator
 */
public class Helper {
    public static void printArray(int[] arrayToBePrinted){
        int arrayLength = arrayToBePrinted.length;
        //System.out.println("");
        for (int i = 0; i < arrayLength; i++) {
            System.out.print(arrayToBePrinted[i] + ",");
        }
        System.out.println();
    }
    
    public static int[] getRandomArrayOfIntegers(int length) {
        int[] input = new int[length];
        for (int i = 0; i < length; i++) {
            input[i] = i + 1;
        }
        
        Random rgen = new Random();
        int randomPosition;
        int temp;
        for(int i = 0; i < length; i++) {
            randomPosition = rgen.nextInt(length);
            temp = input[i];
            input[i] = input[randomPosition];
            input[randomPosition] = temp;
        }
        return input;
    }

    public static int sumArray(int[] input) {
        int sumVal = 0;
        for (int i = 0; i <input.length; i++) {
            sumVal += input[i];
        }
        return sumVal;
    }
}
