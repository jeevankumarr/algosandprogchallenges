/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basicalgorithms;

import Helper.Helper;

/**
 * 
 * @author Jeevan Kumar <jeevan.kr@gmail.com>
 */
public class InsertionSorter {
    public int[] sort(int[] inputArray) {
        int inputArrayLength = inputArray.length;
        int temp;
        int swapCount = 0;
        for (int i = 1; i < inputArrayLength; i++) {
            for (int j = i ; j > 0 && inputArray[j-1] > inputArray[j]; j--) {
                temp = inputArray[j];
                inputArray[j] = inputArray[j-1];
                inputArray[j-1] = temp;
            }
        }
        return inputArray;
    }
    
    public static void testInsertionSorter() {
        int[] input;
        //input = Helper.getRandomArrayOfIntegers(10);
        input = new int[] {8,4,6,7,1,3,9,10,2,5};
        System.out.println("Input Array:");
        Helper.printArray(input);
        
        input = (new InsertionSorter()).sort(input);
        
        System.out.println("Output Array:");
        Helper.printArray(input);
    }
}
