/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package basicalgorithms;

import Helper.Helper;
import java.util.Random;

/**
 *
 * @author Jeevan Kumar
 */
public class FaltuSort {
    int[] sortArray;
    
    public FaltuSort(int[] sortArray) {
        this.sortArray = sortArray;
    }
    
    public int[] sort(int[] input) {
        int[] retVal = null;
        if (input == null) {
            input = sortArray;
        }
        int inputArrayLength = input.length;
        int temp;
        for (int i = 0; i < inputArrayLength; i++) {
            for (int j = i; j < inputArrayLength; j++) {
                if ( input[j] < input[i] ) {
                    temp = input[j];
                    input[j] = input[i];
                    input[i] = temp;
                }
            }
        }
        retVal = input;
        return retVal;
    }
    
    public static void testFaltuSort() {
        int size = 25;
        int[] input = Helper.getRandomArrayOfIntegers(size);
        System.out.println("Input Array:");
        Helper.printArray(input);
        input = (new FaltuSort(input)).sort(null);
        
        System.out.println("Output Array:");
        Helper.printArray(input);
        
        int sum = Helper.sumArray(input);
        boolean sorted = true;
        int test;
        for (int i = 0; i < input.length - 1; i++) {
            test = input[i+1] - input[i];
            if (test!=1) {
                sorted = false;
                break;
            }
        }
        
        if (sum == ( size * ( size + 1 ) / 2) && sorted) {
            System.out.println("Array is sorted");
        } else {
            System.out.println("Array is not sorted");
        }
    }
}
