/*
 * Copyright 2014 Jeevan.Kumar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package otheralgorithms;

import java.util.Random;

/**
 * This class attempts to calculate the value of pi through Monte Carlo method. 
 * The program generates random samples for x and y co-ordinate of a quadrant,
 * then calculates the ratio of the no. of points inside the quadrant to outside.
 * This ratio represents the area of the quadrant of radius 1 with that of a
 * square with radius 1. This ratio yields a value of pi/4. 
 * 
 * @author Jeevan.Kumar
 */
public class MonteCarloPiCalculator {
    
    public double calculatePi(double samples) {
        
        Random xRand = new Random();
        
        //double samples = 100000000d;
        double x,y;
        double testVal;
        double countInside = 0;
        for (int i = 0 ; i < samples; i++) {
            x = xRand.nextDouble();
            y = xRand.nextDouble();
            testVal = x * x + y * y;
            if(testVal < 1) {
                countInside++;
            }
        }
        double pi = countInside / samples * 4.0;
        //System.out.println(pi);
        return pi;
    }
    
    public static void testCalculatePi() {
        double pi = (new MonteCarloPiCalculator()).calculatePi(100000000d);
        System.out.println(pi);
    }
}
